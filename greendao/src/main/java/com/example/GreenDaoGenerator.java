package com.greendao;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class GreenDaoGenerator {

    private static final String PROJECT_DIR = System.getProperty("user.dir");

    public static void main(String[] args) {

        Schema schema = new Schema(1, "simple.br.footsteps.dao");

        Entity process = schema.addEntity("FootSteps");
        process.addIdProperty().primaryKey().autoincrement();
        process.addStringProperty("gid").notNull();
        process.addLongProperty("lon").notNull();
        process.addLongProperty("lat").notNull();
        process.addDateProperty("date").notNull();

        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR + "\\app\\src\\main\\java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
