package simple.br.footsteps.config;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.lang.ref.WeakReference;
import java.util.Date;

import simple.br.footsteps.R;
import simple.br.footsteps.dao.Database;
import simple.br.footsteps.utils.Utils;

/**
 * Project FootSteps
 * Created by Thiago on 25/08/2016.
 */
public class Preferences {

    private static final String sPrefName = "config";
    private static final String sGeneratedId = "generatedIdKey";
    private static final String sStartCountFlag = "startCount";

    private SharedPreferences mShared;
    private WeakReference<Context> mWContext;

    public Preferences(final Context context) {
        mWContext = new WeakReference<>(context);
        mShared = mWContext.get().getSharedPreferences(sPrefName, Activity.MODE_PRIVATE);
    }

    /**
     * Generate a new GID and save it
     * @return the amount of existent GIDs
     */
    public long renewGenerateId() {
        // TODO Check if date is really necessary
        long id = Database.getInstance(mWContext.get()).createNewRecord();
        SharedPreferences.Editor edit = mShared.edit();
        edit.putLong(sGeneratedId, id);
        edit.apply();
        return id;
    }

    /**
     * Returns the last generated Record to be used as key to the steps
     * @return Record (GID)
     */
    public long getGeneratedId() {
        int id = mShared.getInt(sGeneratedId, -1);
        return id == -1 ? renewGenerateId() : id;
    }

    /**
     * Set the record flag. Use this method to start recording the steps
     * @param value
     */
    public void setRecordFlagTo(boolean value) {
        SharedPreferences.Editor edit = mShared.edit();
        edit.putBoolean(sStartCountFlag, value);
        edit.apply();
    }

    /**
     * Returns if it is recording
     * @return boolean the state of the flag
     */
    public boolean getRecordFlag() {
        return mShared.getBoolean(sStartCountFlag, false);
    }
}