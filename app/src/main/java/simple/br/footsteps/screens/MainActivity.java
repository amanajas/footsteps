package simple.br.footsteps.screens;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import simple.br.footsteps.R;
import simple.br.footsteps.config.Preferences;
import simple.br.footsteps.dao.Database;
import simple.br.footsteps.utils.ServiceUtil;

public class MainActivity extends AppCompatActivity {

    private static final int sWaitingForPermission = 50116;

    private ImageButton mButtonStartCount;
    private ImageButton mButtonStopCount;
    private Preferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonStartCount = (ImageButton) findViewById(R.id.buttonStartCount);
        mButtonStopCount = (ImageButton) findViewById(R.id.buttonStopCount);

        mButtonStartCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) &&
                            ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION)) {

                        // Show an expanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                        checkIfLocationIsEnabled();

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.ACCESS_COARSE_LOCATION},
                                sWaitingForPermission);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
            }
        });

        mButtonStopCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.stopCounting();
            }
        });

        mPreferences = new Preferences(getApplicationContext());
        if (mPreferences.getRecordFlag()) {
            mButtonStopCount.setVisibility(View.VISIBLE);
            mButtonStartCount.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case sWaitingForPermission: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkIfLocationIsEnabled();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getApplicationContext(),
                            R.string.permission_denied, Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    /**
     * Stops the counting process
     */
    public void stopCounting() {
        mButtonStopCount.setVisibility(View.GONE);
        mButtonStartCount.setVisibility(View.VISIBLE);
        mPreferences.setRecordFlagTo(false);
        ServiceUtil.stop(getApplicationContext());
        Toast.makeText(this, String.valueOf(Database.getInstance(this).getSteps().size()),
                Toast.LENGTH_LONG).show();
    }

    /**
     * Starts the counting process
     */
    public void startCounting() {
        mButtonStopCount.setVisibility(View.VISIBLE);
        mButtonStartCount.setVisibility(View.GONE);
        mPreferences.renewGenerateId();
        mPreferences.setRecordFlagTo(true);
        ServiceUtil.start(getApplicationContext());
    }

    /**
     * Check if the device is with the location enabled
     */
    public void checkIfLocationIsEnabled() {
        LocationManager lm = (LocationManager) getApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled || !network_enabled) {
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
            Toast.makeText(getApplicationContext(),
                    R.string.location_activated, Toast.LENGTH_LONG).show();
        }

        // Start counting
        startCounting();
    }
}
