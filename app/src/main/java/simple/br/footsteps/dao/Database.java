package simple.br.footsteps.dao;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Date;
import java.util.List;

/**
 * Use this class to deal with the database
 *
 * Project FootSteps
 * Created by Thiago on 29/09/2016.
 */
public class Database {

    private DaoMaster.DevOpenHelper mHelper;
    private DaoMaster mDaoMaster;
    private static final String sDbName = "steps_db";

    private static final Object sLock = new Object();
    private static volatile Database sInstance;

    /**
     * Initializer
     * @param context
     */
    private Database(final Context context) {
        mHelper = new DaoMaster.DevOpenHelper(context,
                sDbName, null);
        mDaoMaster = new DaoMaster(mHelper.getWritableDatabase());
    }

    public static Database getInstance(Context context) {
        Database r = sInstance;
        if (r == null) {
            synchronized (sLock) {    // While we were waiting for the sLock, another
                r = sInstance;        // thread may have instantiated the object.
                if (r == null) {
                    r = new Database(context);
                    sInstance = r;
                }
            }
        }
        return r;
    }

    /**
     * Returns the database session which can be used to do the database tasks
     * @return DaoSession
     */
    private DaoSession getSession() {
        return mDaoMaster.newSession();
    }

    /**
     * Inserts the step into the database
     * @param lt
     * @param lg
     * @param date
     */
    public void insertStep(long recordId, double lt, double lg, Date date) {
        DaoSession session = getSession();
        FootStepsDao dao = session.getFootStepsDao();
        FootSteps step = new FootSteps();
        step.setRecordId(recordId);
        step.setLat(lt);
        step.setLng(lg);
        step.setDate(date);
        dao.insert(step);
        session.clear();
    }

    /**
     * Inserts the record into the database
     * @return the Record row ID
     */
    public long createNewRecord() {
        DaoSession session = getSession();
        StepRecordDao dao = session.getStepRecordDao();
        StepRecord record = new StepRecord();
        // TODO Check if name property is necessary or change it to date
        record.setCreated(new Date());
        long id = dao.insert(record);
        session.clear();
        return id;
    }

    // TODO: Get steps by date
    /**
     * Returns the footsteps by the generated ID
     * @return List of FootSteps
     */
    public List<FootSteps> getSteps() {
        DaoSession session = getSession();
        FootStepsDao dao = session.getFootStepsDao();
        QueryBuilder<FootSteps> query = dao.queryBuilder();
        query.orderAsc(FootStepsDao.Properties.Id);
        session.clear();
        return query.list();
    }
}
