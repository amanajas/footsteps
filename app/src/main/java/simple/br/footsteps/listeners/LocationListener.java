package simple.br.footsteps.listeners;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import java.lang.ref.WeakReference;
import java.util.Date;

import simple.br.footsteps.config.Preferences;
import simple.br.footsteps.dao.Database;
import simple.br.footsteps.logger.Log;

/**
 * Project FootSteps
 * Created by Thiago on 28/09/2016.
 */
public class LocationListener implements android.location.LocationListener {

    public static final String TAG = "Location";

    private Location mLastLocation;
    private Preferences mPreference;
    private Database mDatabase;

    public LocationListener(String provider, Database database, Preferences preferences)
    {
        Log.e(TAG, "LocationListener " + provider);
        mLastLocation = new Location(provider);
        mPreference = preferences;
        mDatabase = database;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "onLocationChanged: " + location);
        mLastLocation.set(location);
        if (mPreference.getRecordFlag()) {
            mDatabase.insertStep(
                    mPreference.getGeneratedId(),
                    mLastLocation.getLatitude(),
                    mLastLocation.getLongitude(),
                    new Date(mLastLocation.getTime()));
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.e(TAG, "onProviderDisabled: " + provider);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.e(TAG, "onProviderEnabled: " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.e(TAG, "onStatusChanged: " + provider);
    }
}
