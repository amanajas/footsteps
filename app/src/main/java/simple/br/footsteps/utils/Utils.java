package simple.br.footsteps.utils;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import simple.br.footsteps.R;

/**
 * Project FootSteps
 * Created by Thiago on 29/09/2016.
 */
public class Utils {

    /**
     * Returns a formatted date
     * @param context
     * @param date
     * @return String
     */
    public static String getDateToString(final Context context, final Date date) {
        final String dateFormat = context.getResources().getString(R.string.date_format);
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
        return format.format(date);
    }
}
