package simple.br.footsteps.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import simple.br.footsteps.services.WalkService;

/**
 * Project FootSteps
 * Created by Thiago on 29/08/2016.
 */
public class ServiceUtil {

    public static boolean isRunning(final Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (WalkService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void start(Context context) {
        if (!isRunning(context)) {
            Intent service = new Intent(context, WalkService.class);
            context.startService(service);
        }
    }

    public static void stop(Context context) {
        if (isRunning(context)) {
            Intent service = new Intent(context, WalkService.class);
            context.stopService(service);
        }
    }
}
