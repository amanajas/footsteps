package com.greendao;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;
import org.greenrobot.greendao.generator.ToMany;

public class DatabaseGenerator {

    private static final String PROJECT_DIR = System.getProperty("user.dir");

    public static void main(String[] args) {
        Schema schema = new Schema(1, "simple.br.footsteps.dao");
        schema.enableKeepSectionsByDefault();

        Entity record = schema.addEntity("StepRecord");
        record.addIdProperty().primaryKey().autoincrement();
        record.addDateProperty("created").notNull().getProperty();

        Entity process = schema.addEntity("FootSteps");
        process.addIdProperty().primaryKey().autoincrement();
        process.addDoubleProperty("lat").notNull();
        process.addDoubleProperty("lng").notNull();

        Property processDate = process.addDateProperty("date").notNull().getProperty();
        Property processRecordId = process.addLongProperty("recordId").notNull().getProperty();

        ToMany recordsToSteps = record.addToMany(process, processRecordId);
        recordsToSteps.setName("steps");
        recordsToSteps.orderAsc(processDate);

        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR + "/app/src/main/java/");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
